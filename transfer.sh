
BB_ROOT="https://bitbucket.org/alexandrerua/arch_install/raw/HEAD"

mkdir -p /root/wallpaper
mkdir -p /root/config/openbox
mkdir -p /root/config/tint2
mkdir -p /root/config/conky
mkdir -p /root/config/lxterminal
mkdir -p /root/config/dmenu
mkdir -p /root/themes/Simple/openbox-3

cd /root
wget ${BB_ROOT}/main.sh
chmod +x main.sh
wget ${BB_ROOT}/main.py
chmod +x main.py

cd /root/wallpaper
wget ${BB_ROOT}/wallpaper/Blade.jpg
wget ${BB_ROOT}/wallpaper/Greens.jpg
wget ${BB_ROOT}/wallpaper/autumn_forest_park_128379_1920x1080.jpg
wget ${BB_ROOT}/wallpaper/outono1.jpg
wget ${BB_ROOT}/wallpaper/river_view.jpg
wget ${BB_ROOT}/wallpaper/rockymountains.jpg

cd /root/config/openbox
wget ${BB_ROOT}/config/openbox/autostart
wget ${BB_ROOT}/config/openbox/menu.xml
wget ${BB_ROOT}/config/openbox/rc.xml

cd /root/themes/Simple/openbox-3
wget ${BB_ROOT}/themes/Simple/openbox-3/themerc 

cd /root/config/tint2
wget ${BB_ROOT}/config/tint2/tint2rc                           

cd /root/config/conky
wget ${BB_ROOT}/config/conky/conky.conf

cd /root/config/lxterminal
wget ${BB_ROOT}/config/lxterminal/lxterminal.conf

cd /root/config/demnu
wget ${BB_ROOT}/config/dmenu/dmenu-bind.sh

cd /root