# arch install script
# Alexandre Rua

# global vars
KEYS='pt-latin1'
DEVICE='/dev/sda'
SWAP_SIZE=''
TIMEZONE='/usr/share/zoneinfo/Europe/Lisbon'
LOCALE='en_US.UTF-8 UTF-8'
HOSTNAME='dev1'
ROOT_PASSWORD=''
USER_NAME='alexandrerua'
USER_PASSWORD=''


RED='\033[0;31m'
GREEN='\033[0;32m'
LIGHT_BLUE='\033[1;34m'
NC='\033[0m'


function warning() {
    echo -e "${LIGHT_BLUE}Welcome to Arch Linux Install Script${NC}"
    echo ""
    echo -e "${RED}Warning"'!'"${NC}"
    echo -e "${RED}This script deletes all partitions of the persistent${NC}"
    echo -e "${RED}storage and continuing all your data in it will be lost.${NC}"
    echo ""
    read -p "Do you want to continue? [y/N] " yn
    case $yn in
        [Yy]* )
            ;;
        * )
            exit
            ;;
    esac
}

function header() {
    echo -e "\n${GREEN}$1${NC}\n"
}

function partition_disk() {
    header "partition disk step"

    sgdisk --zap-all $DEVICE
    parted -s $DEVICE mklabel msdos mkpart primary ext4 2048s 100% set 1 boot on

    #format the partitions
    wipefs -a ${DEVICE}1
    mkfs.ext4 -L root ${DEVICE}1

    # mount the partitions
    mount ${DEVICE}1 /mnt

    # create swapfile if necessary
    if [ -n "$SWAP_SIZE" ]; then
        fallocate -l $SWAP_SIZE /mnt/swap
        chmod 600 /mnt/swap
        mkswap /mnt/swap
    fi


}

function install() {
    header "install step"

    sed -i 's/#Color/Color/' /etc/pacman.conf
    sed -i 's/#TotalDownload/TotalDownload/' /etc/pacman.conf

    pacstrap /mnt base base-devel linux linux-firmware nano htop grub fish openssh
}

function config() {
    header "Configuration step"

    genfstab -U /mnt >> /mnt/etc/fstab

    if [ -n "$SWAP_SIZE" ]; then
        echo "# swap" >> /mnt/etc/fstab
        echo "/swap none swap defaults 0 0" >> /mnt/etc/fstab
        echo "" >> /mnt/etc/fstab
    fi

    arch-chroot /mnt ln -sf $TIMEZONE /etc/localtime
    arch-chroot /mnt hwclock --systohc;
    arch-chroot /mnt timedatectl set-ntp true;
    sed -i "s/#$LOCALE/$LOCALE/" /mnt/etc/locale.gen;
    arch-chroot /mnt locale-gen;
    printf "LANG=$LANG\n" > /mnt/etc/locale.conf;
    printf "KEYMAP=$KEYS\n" > /mnt/etc/vconsole.conf;
    printf "$ROOT_PASSWORD\n$ROOT_PASSWORD" | arch-chroot /mnt passwd
    

}

function users() {
    header "Create Users step"

    # arch-chroot /mnt pacman -S --noconfirm fish;
    sed -i 's/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' /mnt/etc/sudoers
    
    arch-chroot /mnt useradd -m -G users,wheel,storage,optical -g users -s /bin/fish $USER_NAME;
    printf "$USER_PASSWORD\n$USER_PASSWORD" | arch-chroot /mnt passwd $USER_NAME;
}


function network() {
    header "Network step"

    printf "${HOSTNAME}\n" > /mnt/etc/hostname
    printf "127.0.0.1\tlocalhost\n" > /mnt/etc/hosts
    printf "::1\tlocalhost\n" >> /mnt/etc/hosts
    printf "127.0.1.1\t${HOSTNAME}.localdomain\t${HOSTNAME}\n" >> /mnt/etc/hosts

    printf "[Match]\nType=ether\n\n" > /mnt/etc/systemd/network/10-wired.network
    printf "[Network]\nDHCP=yes\n" >> /mnt/etc/systemd/network/10-wired.network

    arch-chroot /mnt systemctl enable systemd-networkd.service 
    arch-chroot /mnt systemctl enable systemd-resolved.service;
    arch-chroot /mnt systemctl enable sshd.service
}

function bootloader() {
    header "Bootloader step"

    # arch-chroot /mnt pacman -S --noconfirm grub;
    arch-chroot /mnt grub-install --target=i386-pc ${DEVICE};
    arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg;
}


set -x

warning
partition_disk
install
config
network
users
bootloader
