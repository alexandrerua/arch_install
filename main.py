#!/usr/bin/python

import subprocess
import textwrap
import re


class settings:
    KEYS = 'pt-latin1'
    DEVICE = '/dev/sda'
    SWAP_SIZE=''
    # https://www.archlinux.org/mirrors/status/
    PACMAN_MIRROR='https://mirror.osbeck.com/archlinux/$repo/os/$arch'  
    TIMEZONE = '/usr/share/zoneinfo/Europe/Lisbon'
    LOCALE = 'en_US.UTF-8 UTF-8'
    HOSTNAME = 'dev1'
    ROOT_PASSWORD = ''
    USER_NAME = 'alexandrerua'
    USER_PASSWORD = ''    
    INTERACTIVE = True

class term:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def printc(text, color):
    print(color + text + term.ENDC)

def get_output(command):
    r = subprocess.run(command, shell=True, stdout=subprocess.PIPE)
    return r.stdout.decode('utf-8')

def _(commands):
    for command in textwrap.dedent(commands).replace('\n',' ').split(';'):
        command = command.strip()
        if command:
            printc(command, term.BOLD)
            subprocess.run(command, shell=True)

def fwrite(fn, txt):
    printc(f'create file: {fn}', term.BOLD)
    txt = textwrap.dedent(txt)
    if txt.endswith('\n'):
        txt = txt[:-1]
    with open(fn, 'w') as f:
        f.write(txt)

def step(func):
    def wrapper(self):
        printc('\n' + func.__name__, term.OKGREEN)
        if settings.INTERACTIVE and input('Proceed [Y/n]') == 'n':
            return
        return func(self)
    return wrapper

class Installer:
    def __init__(self):
        self.CPU_INTEL = 'GenuineIntel' in get_output('lscpu')
        self.VIRTUALBOX = 'virtualbox' in get_output('lspci').lower()
        self.LANG = settings.LOCALE.split(' ')[0]
        print('CPU_INTEL', self.CPU_INTEL)
        print('VIRTUALBOX', self.VIRTUALBOX)
        print('LANG', self.LANG)

    def warning(self):
        printc('Welcome to Arch Linux Install Script\n', term.OKBLUE)
        printc('WARNING !', term.FAIL)
        printc('This script deletes all partitions of the persistent', term.FAIL)
        printc('storage and continuing all your data in it will be lost.\n', term.FAIL)
        print()
        yn = input('Do you want to continue? [y/N] ')
        if yn != 'y':
            exit()

    def pacman_install(self, pkgs):
        _('arch-chroot /mnt pacman -Syu --noconfirm --needed ' + pkgs)

    @step
    def partition_step(self):
        DEVICE = settings.DEVICE
        if re.match('^/dev/nmve', DEVICE) or re.match('^/dev/mmc', DEVICE):
            DEVICE1 = DEVICE + 'p1'
            DEVICE2 = DEVICE + 'p2'
        else:
            DEVICE1 = DEVICE + '1'
            DEVICE2 = DEVICE + '2'


        _(f'''
        sgdisk --zap-all {DEVICE};
        parted -s {DEVICE} 
        mklabel gpt 
        mkpart '"EFI System"' fat32 1Mib 512Mb
        mkpart '"Linux Filesystem"' ext4 512Mb 100%
        set 1 boot on set 1 esp on;    
        
        wipefs -a {DEVICE1};
        wipefs -a {DEVICE2};
        mkfs.fat -n ESP -F32 {DEVICE1};
        mkfs.ext4 -L root {DEVICE2};

        mount {DEVICE2} /mnt;
        mkdir /mnt/boot;
        mount {DEVICE1} /mnt/boot;
        ''')

        # create swapfile if necessary
        if settings.SWAP_SIZE:
            _(f'''
            fallocate -l {settings.SWAP_SIZE} /mnt/swap;
            chmod 600 /mnt/swap;
            mkswap /mnt/swap;
            ''')

        self.BOOT_PARTUUID=get_output(f'blkid -s PARTUUID -o value {DEVICE1}')
        self.ROOT_PARTUUID=get_output(f'blkid -s PARTUUID -o value {DEVICE2}')

    @step
    def install_step(self):
        if settings.PACMAN_MIRROR:
            fwrite('/etc/pacman.d/mirrorlist', f'''\
                Server={settings.PACMAN_MIRROR}
                Server=http://iad.mirror.rackspace.com/archlinux/$repo/os/$arch
            ''')

        _('''
        sed -i 's/#Color/Color/' /etc/pacman.conf;
        sed -i 's/#TotalDownload/TotalDownload/' /etc/pacman.conf;

        pacstrap /mnt base base-devel linux linux-firmware man-db man-pages texinfo nano htop;
        
        sed -i 's/#Color/Color/' /mnt/etc/pacman.conf;
        sed -i 's/#TotalDownload/TotalDownload/' /mnt/etc/pacman.conf;
        ''')

    @step
    def configuration_step(self):
        with open('/mnt/etc/fstab', 'a') as f:
            f.write(get_output('genfstab -U /mnt'))
            if settings.SWAP_SIZE:
                f.write('# swap\n')
                f.write('/swap\tnone\tswap\tdefaults\t0 0')

        _(f'''
        arch-chroot /mnt ln -s -f {settings.TIMEZONE} /etc/localtime;
        arch-chroot /mnt hwclock --systohc;
        arch-chroot /mnt timedatectl set-ntp true;
        sed -i "s/#{settings.LOCALE}/{settings.LOCALE}/" /mnt/etc/locale.gen;
        arch-chroot /mnt locale-gen;
        echo "LANG={self.LANG}" > /mnt/etc/locale.conf;
        echo "KEYMAP={settings.KEYS}" > /mnt/etc/vconsole.conf;

        printf "{settings.ROOT_PASSWORD}\\n{settings.ROOT_PASSWORD}" | arch-chroot /mnt passwd;
        ''')

    @step
    def network_step(self):
        fwrite('/mnt/etc/hostname', f'{settings.HOSTNAME}')
        fwrite('/mnt/etc/hosts', f'''\
            127.0.0.1\tlocalhost
            ::1\tlocalhost
            127.0.1.1\t{settings.HOSTNAME}.localdomain\t{settings.HOSTNAME}
        ''')
        self.pacman_install("networkmanager")
        _('arch-chroot /mnt systemctl enable NetworkManager.service')

    @step
    def virtualbox_step(self):
        if self.VIRTUALBOX:
            self.pacman_install("virtualbox-guest-utils virtualbox-guest-modules-arch")
        
    @step
    def bootloader_step(self):
        INITRD_MICROCODE = ''        
        if self.CPU_INTEL and not self.VIRTUALBOX:
            self.pacman_install("intel-ucode")
            INITRD_MICROCODE = 'initrd /intel-ucode.img'

        _('arch-chroot /mnt bootctl --path=/boot install')
        _('mkdir -p /mnt/etc/pacman.d/hooks/')
        fwrite('/mnt/etc/pacman.d/hooks/systemd-boot.hook', f'''\
            [Trigger]
            Type = Package
            Operation = Upgrade
            Target = systemd
            
            [Action]
            Description = Updating systemd-boot...
            When = PostTransaction
            Exec = /usr/bin/bootctl update
        ''')

        _('mkdir -p /mnt/boot/loader/entries/')
        fwrite('/mnt/boot/loader/loader.conf',f'''\
            # alis
            default archlinux
            timeout 3
            editor no
        ''')
        fwrite('/mnt/boot/loader/entries/archlinux.conf', f'''\
            title Arch Linux
            efi /vmlinuz-linux
            {INITRD_MICROCODE}
            initrd /initramfs-linux.img
            options initrd=initramfs-linux.img root=PARTUUID={self.ROOT_PARTUUID} rw
        ''')
        fwrite('/mnt/boot/loader/entries/archlinux-fallback.conf', f'''\
            title Arch Linux (fallback)
            efi /vmlinuz-linux
            {INITRD_MICROCODE}
            initrd /initramfs-linux-fallback.img
            options initrd=initramfs-linux-fallback.img root=PARTUUID={self.ROOT_PARTUUID} rw
        ''')

        if self.VIRTUALBOX:
            fwrite('/mnt/boot/startup.nsh', '\EFI\systemd\systemd-bootx64.efi')

    @step
    def deskenv_step(self):
        self.pacman_install("xorg-server xorg-xinit xorg-drivers xorg-xrandr")
        self.pacman_install("ttf-bitstream-vera")
        self.pacman_install("openbox tint2 feh archlinux-xdg-menu conky picom network-manager-applet volumeicon obconf")
        self.pacman_install("lxterminal pcmanfm geany dmenu alsa-utils")
        self.pacman_install("chromium firefox")
        self.pacman_install("mupdf vlc")

        _('''\
        cp /root/wallpaper /mnt/usr/share/ -r;
        cp /root/themes/* /mnt/usr/share/themes -r;
        mkdir -p /mnt/etc/skel/.config;
        cp /root/config/* /mnt/etc/skel/.config -r;
        ''')

        fwrite('/mnt/etc/skel/.xinitrc', '''\
        #!/bin/bash

        export DE=openbox

        # Dbus fix
        if [ -z "$DBUS_SESSION_BUS_ADDRESS" ]; then
            eval "$(dbus-launch --sh-syntax --exit-with-session)"
        fi

        /usr/bin/xdg-user-dirs-update

        exec openbox-session
        ''')

    @step
    def devtools_step(self):
        self.pacman_install("openssh git python python-pip python-pipenv code")
        _('arch-chroot /mnt pip install ipython')

        if not self.VIRTUALBOX:
            # stand alone install
            # install virtualization
            self.pacman_install("virtualbox virtualbox-host-modules-arch virtualbox-guest-iso")
            fwrite('/usr/lib/modules-load.d/virtualbox-host-modules-arch.conf','''\
                vboxdrv
                vboxnetadp
                vboxnetflt
                vboxpci
                ''')

            self.pacman_install("docker")
            _('arch-chroot /mnt systemctl enable docker.service')

    @step
    def users_step(self):
        self.pacman_install("xdg-user-dirs fish")
        _("sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /mnt/etc/sudoers")
        _(f'arch-chroot /mnt useradd -m -G users,wheel,storage,optical -g users -s /bin/fish {settings.USER_NAME}')
        _(f'printf "{settings.USER_PASSWORD}\\n{settings.USER_PASSWORD}" | arch-chroot /mnt passwd {settings.USER_NAME}')
        fwrite(f'/mnt/home/{settings.USER_NAME}/runonce.sh', '''\
        #!/bin/bash
        ssh-keygen
        git config --global user.email "alexandrerua@gmail.com"
        git config --global user.name "Alexandre Rua"
        ''')

    def run(self):
        self.warning()
        self.partition_step()
        self.install_step()
        self.configuration_step()
        self.network_step()
        self.virtualbox_step()
        self.bootloader_step()
        self.deskenv_step()
        self.devtools_step()
        self.users_step()
        printc("done installing arch linux", term.OKBLUE)


inst = Installer()
inst.run()