# arch install script
# Alexandre Rua

# global vars
KEYS='pt-latin1'
DEVICE='/dev/sda'
SWAP_SIZE=''
PACMAN_MIRROR='https://mirror.osbeck.com/archlinux/$repo/os/$arch'  # https://www.archlinux.org/mirrors/status/
TIMEZONE='/usr/share/zoneinfo/Europe/Lisbon'
LOCALE='en_US.UTF-8 UTF-8'

HOSTNAME='dev1'

ROOT_PASSWORD=''
USER_NAME='alexandrerua'
USER_PASSWORD=''




# dont edit below this line
BOOT_PARTUUID=''
ROOT_PARTUUID=''
LANG=$(echo $LOCALE | cut -d' ' -f1)
CPU_INTEL=''
VIRTUALBOX=''


RED='\033[0;31m'
GREEN='\033[0;32m'
LIGHT_BLUE='\033[1;34m'
NC='\033[0m'


function warning() {
    echo -e "${LIGHT_BLUE}Welcome to Arch Linux Install Script${NC}"
    echo ""
    echo -e "${RED}Warning"'!'"${NC}"
    echo -e "${RED}This script deletes all partitions of the persistent${NC}"
    echo -e "${RED}storage and continuing all your data in it will be lost.${NC}"
    echo ""
    read -p "Do you want to continue? [y/N] " yn
    case $yn in
        [Yy]* )
            ;;
        [Nn]* )
            exit
            ;;
        * )
            exit
            ;;
    esac
}


function header() {
    echo ""
    echo -e "${GREEN}$1${NC}"
    echo ""
}

function pacman_install() {
    arch-chroot /mnt pacman -Syu --noconfirm --needed $1
}


function facts() {
    if [ -n "$(lscpu | grep GenuineIntel)" ]; then
        CPU_INTEL="true"
    fi

    if [ -n "$(lspci | grep -i virtualbox)" ]; then
        VIRTUALBOX="true"
    fi
}


function partition_disk() {
    header "partition disk step"

    sgdisk --zap-all $DEVICE
    parted -s $DEVICE mklabel gpt
    parted -s $DEVICE mkpart '"EFI System"' fat32 1Mib 512Mib
    parted -s $DEVICE mkpart '"Linux Filesystem"' ext4 512Mib 100%
    parted -s $DEVICE set 1 boot on set 1 esp on

    #format the partitions
    wipefs -a ${DEVICE}1
    wipefs -a ${DEVICE}2
    mkfs.fat -n ESP -F32 ${DEVICE}1
    mkfs.ext4 -L root ${DEVICE}2

    # mount the partitions
    mount ${DEVICE}2 /mnt
    mkdir /mnt/boot
    mount ${DEVICE}1 /mnt/boot

    # create swapfile if necessary
    if [ -n "$SWAP_SIZE" ]; then
        fallocate -l $SWAP_SIZE /mnt/swap
        chmod 600 /mnt/swap
        mkswap /mnt/swap
    fi

    BOOT_PARTUUID=$(blkid -s PARTUUID -o value ${DEVICE}1)
    ROOT_PARTUUID=$(blkid -s PARTUUID -o value ${DEVICE}2)
}


function install() {
    header "install step"

    if [ -n "$PACMAN_MIRROR" ]; then
        echo "Server=$PACMAN_MIRROR" > /etc/pacman.d/mirrorlist
        echo 'Server=http://iad.mirror.rackspace.com/archlinux/$repo/os/$arch' >> etc/pacman.d/mirrorlist
    fi
    sed -i 's/#Color/Color/' /etc/pacman.conf
    sed -i 's/#TotalDownload/TotalDownload/' /etc/pacman.conf

    pacstrap /mnt base base-devel linux linux-firmware man-db man-pages texinfo nano htop

    sed -i 's/#Color/Color/' /mnt/etc/pacman.conf
    sed -i 's/#TotalDownload/TotalDownload/' /mnt/etc/pacman.conf
}


function configuration() {
    header "Configuration step"

    genfstab -U /mnt >> /mnt/etc/fstab

    if [ -n "$SWAP_SIZE" ]; then
        echo "# swap" >> /mnt/etc/fstab
        echo "/swap none swap defaults 0 0" >> /mnt/etc/fstab
        echo "" >> /mnt/etc/fstab
    fi

    arch-chroot /mnt ln -s -f $TIMEZONE /etc/localtime
    arch-chroot /mnt hwclock --systohc
    arch-chroot /mnt timedatectl set-ntp true
    sed -i "s/#$LOCALE/$LOCALE/" /mnt/etc/locale.gen
    arch-chroot /mnt locale-gen
    echo -e "LANG=$LANG" > /mnt/etc/locale.conf
    echo -e "KEYMAP=$KEYS" > /mnt/etc/vconsole.conf

    printf "$ROOT_PASSWORD\n$ROOT_PASSWORD" | arch-chroot /mnt passwd
}


function network() {
    header "Network step"

    echo $HOSTNAME > /mnt/etc/hostname
    echo "127.0.0.1\tlocalhost" > /mnt/etc/hosts
    echo "::1\tlocalhost" >> /mnt/etc/hosts
    echo "127.0.1.1\t${HOSTNAME}.localdomain\t${HOSTNAME}" >> /mnt/etc/hosts

    pacman_install "networkmanager"
    arch-chroot /mnt systemctl enable NetworkManager.service
}


function virtualbox() {
    header "VirtualBox step"

    if [ -n $VIRTUALBOX ]; then
        pacman_install "virtualbox-guest-utils virtualbox-guest-modules-arch"
    fi
}

function users() {
    header "Create Users step"

    pacman_install "xdg-user-dirs fish"
    sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /mnt/etc/sudoers
    
    arch-chroot /mnt useradd -m -G users,wheel,storage,optical -g users -s /bin/fish $USER_NAME
    printf "$USER_PASSWORD\n$USER_PASSWORD" | arch-chroot /mnt passwd $USER_NAME
}

function bootloader() {
    header "bootloader step"

    if [ "$CPU_INTEL" == "true" -a "$VIRTUALBOX" != "true" ]; then
        pacman_install "intel-ucode"
    fi
    
    arch-chroot /mnt bootctl --path=/boot install

    arch-chroot /mnt mkdir -p "/boot/loader/"
    arch-chroot /mnt mkdir -p "/boot/loader/entries/"

    echo "# alis" > "/mnt/boot/loader/loader.conf"
    echo "default archlinux" >> "/mnt/boot/loader/loader.conf"
    echo "timeout 3" >> "/mnt/boot/loader/loader.conf"
    echo "editor no" >> "/mnt/boot/loader/loader.conf"

    arch-chroot /mnt mkdir -p "/etc/pacman.d/hooks/"

    echo "[Trigger]" >> /mnt/etc/pacman.d/hooks/systemd-boot.hook
    echo "Type = Package" >> /mnt/etc/pacman.d/hooks/systemd-boot.hook
    echo "Operation = Upgrade" >> /mnt/etc/pacman.d/hooks/systemd-boot.hook
    echo "Target = systemd" >> /mnt/etc/pacman.d/hooks/systemd-boot.hook
    echo "" >> /mnt/etc/pacman.d/hooks/systemd-boot.hook
    echo "[Action]" >> /mnt/etc/pacman.d/hooks/systemd-boot.hook
    echo "Description = Updating systemd-boot..." >> /mnt/etc/pacman.d/hooks/systemd-boot.hook
    echo "When = PostTransaction" >> /mnt/etc/pacman.d/hooks/systemd-boot.hook
    echo "Exec = /usr/bin/bootctl update" >> /mnt/etc/pacman.d/hooks/systemd-boot.hook

    SYSTEMD_MICROCODE=""
    if [ "$CPU_INTEL" == "true" -a "$VIRTUALBOX" != "true" ]; then
        SYSTEMD_MICROCODE="/intel-ucode.img"
    fi

    echo "title Arch Linux" > "/mnt/boot/loader/entries/archlinux.conf"
    echo "efi /vmlinuz-linux" >> "/mnt/boot/loader/entries/archlinux.conf"
    if [ -n "$SYSTEMD_MICROCODE" ]; then
        echo "initrd $SYSTEMD_MICROCODE" >> "/mnt/boot/loader/entries/archlinux.conf"
    fi
    echo "initrd /initramfs-linux.img" >> "/mnt/boot/loader/entries/archlinux.conf"
    echo "options initrd=initramfs-linux.img root=PARTUUID=$ROOT_PARTUUID rw" >> "/mnt/boot/loader/entries/archlinux.conf"

    echo "title Arch Linux (fallback)" > "/mnt/boot/loader/entries/archlinux-fallback.conf"
    echo "efi /vmlinuz-linux" >> "/mnt/boot/loader/entries/archlinux-fallback.conf"
    if [ -n "$SYSTEMD_MICROCODE" ]; then
        echo "initrd $SYSTEMD_MICROCODE" >> "/mnt/boot/loader/entries/archlinux-fallback.conf"
    fi
    echo "initrd /initramfs-linux-fallback.img" >> "/mnt/boot/loader/entries/archlinux-fallback.conf"
    echo "options initrd=initramfs-linux-fallback.img root=PARTUUID=$ROOT_PARTUUID rw" >> "/mnt/boot/loader/entries/archlinux-fallback.conf"

    if [ "$VIRTUALBOX" == "true" ]; then
        echo -n "\EFI\systemd\systemd-bootx64.efi" > "/mnt/boot/startup.nsh"
    fi
}

function deskenv() {
    header "Desktop environment step"

    pacman_install "xorg-server xorg-xinit xorg-drivers"
    pacman_install "openbox tint2 feh archlinux-xdg-menu conky picom network-manager-applet volumeicon obconf"
    pacman_install "lxterminal pcmanfm geany"
    pacman_install "chromium firefox"
    pacman_install "mupdf vlc"

    cp /root/wallpaper /mnt/usr/share/ -r
    cp /root/themes/* /mnt/usr/share/themes -r

    mkdir -p /mnt/etc/skel/.config
    cp /root/config/* /mnt/etc/skel/.config -r
    #mkdir -p /mnt/home/${USER_NAME}/.config
    #cp /root/config/openbox /mnt/home/${USER_NAME}/.config -r
    #cp /root/config/tint2 /mnt/home/${USER_NAME}/.config -r
    #cp /root/config/conky /mnt/home/${USER_NAME}/.config -r
    #arch-chroot /mnt chown ${USER_NAME} /home/${USER_NAME}/.config -R
    #arch-chroot /mnt chgrp users /home/${USER_NAME}/.config -R

    #echo "#!/bin/bash" > /mnt/home/${USER_NAME}/.xinitrc
    #echo "export DE=openbox" >> /mnt/home/${USER_NAME}/.xinitrc
    #echo "exec openbox-session" >> /mnt/home/${USER_NAME}/.xinitrc
    #arch-chroot /mnt chown ${USER_NAME} /home/${USER_NAME}/.xinitrc
    #arch-chroot /mnt chgrp users /home/${USER_NAME}/.xinitrc
}

function devtools() {
    pacman_install "git python python-pip python-pipenv code"
    arch-chroot /mnt pip install ipython
}

loadkeys $KEYS
timedatectl set-ntp true
warning
facts
partition_disk
install
configuration
network
virtualbox
bootloader
deskenv
devtools
users